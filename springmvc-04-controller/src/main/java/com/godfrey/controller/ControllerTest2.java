package com.godfrey.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * description : 前端控制器2
 *
 * @author godfrey
 * @since 2020-05-23
 */
@Controller
public class ControllerTest2 {

    @RequestMapping("/test2")
    public String test1(Model model) {
        model.addAttribute("msg", "ControllerTest2");
        return "test";
    }

    @RequestMapping("/test3")
    public String test3(Model model) {
        model.addAttribute("msg", "ControllerTest3");
        return "test";
    }
}
