package com.godfrey.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * description : ControllerTest3
 *
 * @author godfrey
 * @since 2020-05-23
 */
@Controller
@RequestMapping("/admin")
public class ControllerTest3 {
    @RequestMapping("/h1")
    public String test(){
        return "test";
    }
}
