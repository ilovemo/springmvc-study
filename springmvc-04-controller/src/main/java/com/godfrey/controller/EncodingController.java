package com.godfrey.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * description : Encoding
 *
 * @author godfrey
 * @since 2020-05-23
 */
@Controller
public class EncodingController {
    @RequestMapping("/e/t")
    public String test(Model model, String name) {
        model.addAttribute("msg", name);
        return "test";
    }
}