package com.godfrey.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * description : RestFulController
 *
 * @author godfrey
 * @since 2020-05-23
 */
@Controller
public class RestFulController {

    //映射访问地址
    @RequestMapping("/commit/{p1}/{p2}")
    public String index(@PathVariable int p1, @PathVariable String p2, Model model) {
        String result = p1 + p2;
        model.addAttribute("msg", "结果：" + result);
        return "test";
    }

    //映射访问路径,必须是Get请求
    @RequestMapping(value = "/hello",method = {RequestMethod.GET})
    public String index2(Model model){
        model.addAttribute("msg", "hello!");
        return "test";
    }

}
