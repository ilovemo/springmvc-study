package com.godfrey.controller;

import com.godfrey.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * description : UserController
 *
 * @author godfrey
 * @since 2020-05-23
 */
@Controller
@RequestMapping("/user")
public class UserController {

    //http://localhost:8080/user/t1?name=xxx
    @GetMapping("t1")
    public String test1(String name, Model model) {
        System.out.println("接收到前端参数：" + name);
        model.addAttribute("msg",name);
        return "test";
    }

    //http://localhost:8080/user/t2?username=xxx
    @GetMapping("t2")
    public String test2(@RequestParam("username") String name, Model model) {
        System.out.println("接收到前端参数：" + name);
        model.addAttribute("msg",name);
        return "test";
    }

    //http://localhost:8080/user/user?name=godfrey&id=1&age=15
    @RequestMapping("user")
    public String user(User user){
        System.out.println(user);
        return "test";
    }
}
